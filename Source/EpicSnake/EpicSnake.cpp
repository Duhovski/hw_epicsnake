// Copyright Epic Games, Inc. All Rights Reserved.

#include "EpicSnake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EpicSnake, "EpicSnake" );
