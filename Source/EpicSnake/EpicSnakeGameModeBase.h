// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EpicSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EPICSNAKE_API AEpicSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
